# Cookiecutter template for a data analysis project at LSI

## Usage

```
	pip install cookiecutter
	cookiecutter bb:princeton_genomics/cookiecutter-lsi-project.git
```

You will be prompted for basic information which is used in the template.

## Project Structure

```
	.
	|-- project_description.yaml  <- Project description file for https://bitbucket.org/princeton_genomics/project_archive
	|-- LICENSE
	|-- README.md
	|-- .gitignore
	|-- data                      <- The original, immutable data
	|-- src                       <- Source code for this project
	|-- results                   <- Derived (reproducible) results, not stored in git
```
